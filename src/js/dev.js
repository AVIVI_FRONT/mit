function openSearch() {
    $('.js-search').on('click', function () {
        $('.header__top').toggleClass('search-opened');
    })
}

function hoverLine() {
    $(document).on('mouseover', '[data-hover]', function () {
        var selector = $(this).attr('data-hover');
        $('.'+ selector +' ').addClass('show');
    });
    $(document).on('mouseleave', '[data-hover]', function () {
        var selector = $(this).attr('data-hover');
        $('.'+ selector +' ').removeClass('show');
    });
}
function hoverLink() {
    $(document).on('click', '[data-hover]', function () {
        var selector = $(this).attr('data-hover');
        var href = window.location.href;
        href = href + '/' + selector;
        window.location.href = href;
    })
}
function openMobMenu() {
    $('.mobmenu__burger').on('click', function () {
        $('.mobmenu').toggleClass('opened');
        $('html').toggleClass('mobmenu');
    })
}
function openSubMenu() {
    $('.mobmenu__arrow').on('click', function () {
        $(this).toggleClass('opened').siblings('.mobmenu__submenu').toggleClass('opened').parents('.mobmenu__item').toggleClass('opened');
    })
}
function productTabs() {
    $('.product__tab a:not(active)').on('click', function (e) {
        e.preventDefault();
        var tab = $(this).attr('href');
        $('.product__tab a').removeClass('active');
        $(this).addClass('active');
        $('.product__content').removeClass('active');
        $(tab).addClass('active');
    })
}
function initEventSlider() {
    $('.event__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        dots: true
    })
}

$(document).ready(function () {
    openSearch();
    goTo();
    openMobMenu();
    openSubMenu();
    productTabs();
    initEventSlider();
    if ($(window).width() > 1200) {
        hoverLine();
        hoverLink();
    }
    // var svgHeight = $('.vector').outerHeight();
    // $('.background').css('max-height', svgHeight);
});